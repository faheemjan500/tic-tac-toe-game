/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.ch03tictactoe;

import com.packtpublishing.tddjava.exceptions.BoxOccupiedException;
import com.packtpublishing.tddjava.exceptions.OutOfBoundryException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertEquals;

public class TicTacToeSpec {

  @Rule
  public ExpectedException exception = ExpectedException.none();
  private TicTacToe ticTacToe;

  @Before
  public final void before() {
    ticTacToe = new TicTacToe();
  }

  @Test
  public void checkingXifOutsideTheBoard() throws OutOfBoundryException, BoxOccupiedException {
    exception.expect(OutOfBoundryException.class);
    ticTacToe.play(5, 2);

  }

  @Test
  public void checkingYifOutsideTheBoard() throws OutOfBoundryException, BoxOccupiedException {
    exception.expect(OutOfBoundryException.class);
    ticTacToe.play(2, 5);
  }

  @Test
  public void firstPlayerShouldBeX() {
    assertEquals('X', ticTacToe.nextPlayer());
  }

  @Test
  public void ifPlacedOnOccupiedPlace() throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(2, 1);
    exception.expect(BoxOccupiedException.class);
    ticTacToe.play(2, 1);
  }

  @Test
  public void lastPlayerWasXThenNextPlayerShouldBeO()
    throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(1, 1);
    assertEquals('o', ticTacToe.nextPlayer());
  }

  @Test
  public void whenAllBoxesAreFilledThenDraw() throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(1, 1);
    ticTacToe.play(1, 2);
    ticTacToe.play(1, 3);
    ticTacToe.play(2, 1);
    ticTacToe.play(2, 3);
    ticTacToe.play(2, 2);
    ticTacToe.play(3, 1);
    ticTacToe.play(3, 3);
    final String actual = ticTacToe.play(3, 2);
    assertEquals("The result is draw", actual);
  }

  @Test
  public void whenPlayAndBottomTopDiagonalLineThenWinner()
    throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(1, 3);
    ticTacToe.play(1, 1);
    ticTacToe.play(2, 2);
    ticTacToe.play(1, 2);
    final String actual = ticTacToe.play(3, 1);
    assertEquals("X is the winner", actual);
  }

  @Test
  public void whenPlayAndTopBottomDiagonalLineThenWinner()
    throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(1, 1);
    ticTacToe.play(1, 2);
    ticTacToe.play(2, 2);
    ticTacToe.play(1, 3);
    final String actual = ticTacToe.play(3, 3);
    assertEquals("X is the winner", actual);
  }

  @Test
  public void whenPlayAndWholeHorizontalLineThenWinner()
    throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(1, 1);
    ticTacToe.play(1, 2);
    ticTacToe.play(2, 1);
    ticTacToe.play(2, 2);
    final String actual = ticTacToe.play(3, 1);
    assertEquals("X is the winner", actual);
  }

  @Test
  public void whenPlayAndWholeVerticalLineThenWinner()
    throws OutOfBoundryException, BoxOccupiedException {
    ticTacToe.play(2, 1);
    ticTacToe.play(1, 1);
    ticTacToe.play(3, 1);
    ticTacToe.play(1, 2);
    ticTacToe.play(2, 2);
    final String actual = ticTacToe.play(1, 3);
    assertEquals("o is the winner", actual);
  }

  @Test
  public void whenPlayThenNoWinner() throws OutOfBoundryException, BoxOccupiedException {
    final String actual = ticTacToe.play(1, 1);
    assertEquals("No winner", actual);
  }

}
