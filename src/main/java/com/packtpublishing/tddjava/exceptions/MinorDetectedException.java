/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.exceptions;

class MinorDetectedException extends Exception {
  private static final long serialVersionUID = 1L;
  private final String message;

  public MinorDetectedException(String str2) {
    this.message = str2;
  }

  @Override
  public String toString() {
    return "MinorDetectedException Occurred: " + message;
  }
}