/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.exceptions;

public class BoxOccupiedException extends Exception {

  public BoxOccupiedException() {
    super();
  }

  public BoxOccupiedException(String exceptionMessage) {
    super(exceptionMessage);
  }
}
