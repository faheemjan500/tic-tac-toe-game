/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package com.packtpublishing.tddjava.exceptions;

public class CarNotFoundException extends Exception {

  private String message = null;

  public CarNotFoundException(String str) {
    this.message = str;
  }

  @Override
  public String toString() {
    return "CarNotFoundException occured:" + message;
  }
}
