/*
 * Copyright 2019 Thales Italia spa.
 * 
 * This program is not yet licensed and this file may not be used under any
 * circumstance.
 */
package com.packtpublishing.tddjava.exceptions;

public class CoordinatesStringEnteredException extends Exception {

  public CoordinatesStringEnteredException() {
    super();
  }

  public CoordinatesStringEnteredException(String message) {
    super(message);
  }
}
