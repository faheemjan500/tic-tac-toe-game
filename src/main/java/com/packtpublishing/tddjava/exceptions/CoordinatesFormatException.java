/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.exceptions;

public class CoordinatesFormatException extends Exception {

  public CoordinatesFormatException() {
    super();
  }

  public CoordinatesFormatException(String exceptionMessage) {
    super(exceptionMessage);
  }
}
