/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.ch03tictactoe;

import com.packtpublishing.tddjava.exceptions.BoxOccupiedException;
import com.packtpublishing.tddjava.exceptions.CoordinatesFormatException;
import com.packtpublishing.tddjava.exceptions.CoordinatesStringEnteredException;
import com.packtpublishing.tddjava.exceptions.OutOfBoundryException;

import static com.packtpublishing.tddjava.ch03tictactoe.Utility.*;

import java.util.logging.Level;
import java.util.logging.Logger;

public class AppCheck {
  private enum AXIS {
    X,
    Y
  }

  private static final Logger LOGGER = Logger.getLogger(App.class.getName());;

  public static boolean isNumber(String numberAsString) {
    if (numberAsString == null || numberAsString.length() == 0) {
      return false;
    }
    for (final char c : numberAsString.toCharArray()) {
      if (!Character.isDigit(c)) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    try {
      final MenuManager menuManager = new MenuManager();
      final TicTacToe ticTacToe = new TicTacToe();
      String coordinates = null;
      String gameResult = null;
      menuManager.wellcomeGame();
      char lastPlayer = ticTacToe.nextPlayer();

      while (true) {
        menuManager.toDisplaytheBoard(ticTacToe.getBoard(), ticTacToe.getSize());

        coordinates = menuManager.getInformationFromConsole(lastPlayer);
        try {
          final int x = extractCoordinate(coordinates, AXIS.X);
          final int y = extractCoordinate(coordinates, AXIS.Y);
          gameResult = ticTacToe.play(x, y);
        } catch (final CoordinatesFormatException e) {
          printInConsole("le coordinate devono essere separate da una virgola");
        } catch (final NumberFormatException e) {
          printInConsole("non hai inserito le coordinate numeriche");
        } catch (final OutOfBoundryException e) {
          printInConsole("hai inserito coordinate fuori da quelle consentite");
          // System.out.print(e);
        } catch (final BoxOccupiedException e) {
          printInConsole("box già occiupato");
        } catch (final CoordinatesStringEnteredException e) {
          printInConsole("You entered wrong value, plz try again!");
        }
        if (!"No winner".equalsIgnoreCase(gameResult))
          printInConsole(gameResult + LINE_SEPARATOR);
        final String winner = lastPlayer + " is the winner";
        if (winner.equalsIgnoreCase(gameResult)
            || "The result is draw".equalsIgnoreCase(gameResult)) {
          menuManager.toDisplaytheBoard(ticTacToe.getBoard(), ticTacToe.getSize());
          break;
        }
        lastPlayer = ticTacToe.nextPlayer();
      }
    } catch (

    final RuntimeException ex) {
      printInConsole(
          "E' avvenuto un errore inaspettetato. Per cortesia riavviare il gioco e contattare il servizio clienti");
      LOGGER.log(Level.SEVERE, ex.getMessage(), ex);
    }
  }

  private static int extractCoordinate(String coordinatesFromConsole, AXIS axis)
    throws CoordinatesFormatException, CoordinatesStringEnteredException {
    int coordinate = 0;
    if (coordinatesFromConsole == null) {
      throw new CoordinatesStringEnteredException(
          "you didn't enter anything. pls insert your value!");
    }
    if (coordinatesFromConsole.contains(",") && coordinatesFromConsole.split(",").length == 2) {
      final String[] splittedCoordinatesFromConsole = coordinatesFromConsole.split(",");
      String coordinateFromConsole = splittedCoordinatesFromConsole[0];
      if (axis == AXIS.Y) {
        coordinateFromConsole = splittedCoordinatesFromConsole[1];
      }
      if (isNumber(coordinateFromConsole)) {
        coordinate = Integer.parseInt(coordinateFromConsole);
      }
    } else {
      throw new CoordinatesFormatException("bla bla");
    }
    return coordinate;
  }
}
