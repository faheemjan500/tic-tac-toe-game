/*
 *  * Copyright 2019 Thales Italia spa.  *  * This program is not yet licensed and this file may not
 * be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.ch03tictactoe;

import com.packtpublishing.tddjava.exceptions.BoxOccupiedException;
import com.packtpublishing.tddjava.exceptions.OutOfBoundryException;

import java.util.logging.Level;
import java.util.logging.Logger;

public class TicTacToe {
  private static final int SIZE = 3;
  private static final Logger LOGGER = Logger.getLogger(TicTacToe.class.getName());
  private final Character[][] board;
  private char lastPlayer;

  public TicTacToe() {
    board = new Character[][] {
        {
            '\0', '\0', '\0'
        }, {
            '\0', '\0', '\0'
        }, {
            '\0', '\0', '\0'
        }
    };
    lastPlayer = '\0';
  }

  public boolean isDraw() {
    for (int x = 0; x < SIZE; x++) {
      for (int y = 0; y < SIZE; y++) {
        if (board[x][y] == '\0') {
          return false;
        }
      }
    }
    return true;
  }

  public boolean isWin(int x, int y) {
    final int playerTotal = lastPlayer * SIZE;
    char horizontal;
    char vertical;
    char diagonal1;
    char diagonal2;
    horizontal = vertical = diagonal1 = diagonal2 = '\0';
    for (int i = 0; i < SIZE; i++) {
      horizontal += board[i][y - 1];
      vertical += board[x - 1][i];
      diagonal1 += board[i][i];
      diagonal2 += board[i][SIZE - i - 1];
    }
    if (horizontal == playerTotal || vertical == playerTotal || diagonal1 == playerTotal
        || diagonal2 == playerTotal) {
      return true;
    }
    return false;
  }

  public char nextPlayer() {
    if (lastPlayer == 'X')
      return 'o';
    else
      return 'X';

  }

  public String play(int x, int y) throws OutOfBoundryException, BoxOccupiedException {
    checkAxis(x);
    checkAxis(y);
    lastPlayer = nextPlayer();
    setBox(x, y, lastPlayer);
    if (isWin(x, y)) {
      return lastPlayer + " is the winner";
    } else if (isDraw()) {
      return "The result is draw";
    } else {
      return "No winner";
    }
  }

  private void checkAxis(int axis) throws OutOfBoundryException {
    if (axis < 1 || axis > SIZE) {
      final String errorMessage = "Axis " + axis + " is out of bound";
      LOGGER.log(Level.FINEST, errorMessage);
      throw new OutOfBoundryException(errorMessage);
    }

  }

  private void setBox(int x, int y, char lastPlayer) throws BoxOccupiedException {
    if (board[x - 1][y - 1] != '\0') {
      final String errorMessage = "Boxx " + x + "," + y + " is already occupied";
      LOGGER.log(Level.FINEST, errorMessage);
      throw new BoxOccupiedException(errorMessage);
    } else {
      board[x - 1][y - 1] = lastPlayer;
    }
  }

  Character[][] getBoard() {
    return board;
  }

  int getSize() {
    return SIZE;
  }
}
