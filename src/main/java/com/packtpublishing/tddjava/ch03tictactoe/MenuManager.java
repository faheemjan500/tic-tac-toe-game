/*
 *  * Copyright 2019 Thales Italia spa.  *   * This program is not yet licensed and this file may
 * not be used under any  * circumstance.  
 */
package com.packtpublishing.tddjava.ch03tictactoe;

import static com.packtpublishing.tddjava.ch03tictactoe.Utility.printInConsole;

import java.util.Arrays;
import java.util.Scanner;

public class MenuManager {
  private final Scanner scanner = new Scanner(System.in);

  public String getInformationFromConsole(char currentPlayer) {
    String input = null;
    printInConsole(" Player " + currentPlayer + "  turn :");
    printInConsole("Enter your coordinates (0,0) : ");
    input = scanner.nextLine();
    return input;
  }

  public void wellcomeGame() {
    printInConsole("Wellcome in Tic Tac Toe" + Utility.LINE_SEPARATOR);
  }

  void toDisplaytheBoard(Character[][] board, int sizeOfBoard) {
    for (int rowIndex = 0; rowIndex < sizeOfBoard; rowIndex++) {
      printInConsole(Arrays.toString(board[rowIndex]));
    }
  }

}
